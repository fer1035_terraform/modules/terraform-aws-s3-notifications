resource "aws_sqs_queue" "queue" {
  count = var.queue_id != null ? 1 : 0

  name = "${var.queue_id}-queue"
}

data "aws_iam_policy_document" "queue" {
  count = var.queue_id != null ? 1 : 0

  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["s3.amazonaws.com"]
    }

    actions   = ["sqs:SendMessage"]
    resources = [aws_sqs_queue.queue[0].arn]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"
      values   = ["arn:aws:s3:::${var.bucket_name}"]
    }
  }

  statement {
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        aws_iam_user.user.arn,
        aws_iam_role.role.arn
      ]
    }

    actions = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes",
      "sqs:GetQueueUrl",
      "sqs:ChangeMessageVisibility"
    ]

    resources = [aws_sqs_queue.queue[0].arn]
  }

  depends_on = [
    aws_iam_user.user,
    aws_iam_role.role
  ]
}

resource "aws_sqs_queue_policy" "queue" {
  count = var.queue_id != null ? 1 : 0

  queue_url = aws_sqs_queue.queue[0].id
  policy    = data.aws_iam_policy_document.queue[0].json
}
