resource "aws_iam_user" "user" {
  name = "s3-${var.bucket_name}-notify-user"
  path = var.user_path
}

resource "aws_iam_role" "role" {
  name = "s3-${var.bucket_name}-notiff-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""

        Principal = {
          AWS = var.assume_role_principals != [] ? var.assume_role_principals : ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
        }

        Condition = {
          StringEquals = {
            "sts:ExternalId" = var.external_id != null ? var.external_id : data.aws_caller_identity.current.account_id
          }
        }
      }
    ]
  })
}

data "aws_iam_policy_document" "s3" {
  statement {
    effect = "Allow"

    actions = [
      "s3:ListBucket"
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}"
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject"
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}/*"
    ]
  }
}

resource "aws_iam_policy" "s3" {
  name        = "s3-${var.bucket_name}-notify-s3-policy"
  description = "S3 actions for S3 notifications policy."
  policy      = data.aws_iam_policy_document.s3.json
}

resource "aws_iam_user_policy_attachment" "s3" {
  user       = aws_iam_user.user.name
  policy_arn = aws_iam_policy.s3.arn
}

resource "aws_iam_role_policy_attachment" "s3" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.s3.arn
}

data "aws_iam_policy_document" "sqs" {
  count = var.queue_id != null ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes",
      "sqs:GetQueueUrl",
      "sqs:ChangeMessageVisibility"
    ]

    resources = [
      aws_sqs_queue.queue[0].arn
    ]
  }
}

resource "aws_iam_policy" "sqs" {
  count = var.queue_id != null ? 1 : 0

  name        = "s3-${var.bucket_name}-notify-sqs-policy"
  description = "SQS actions for S3 notifications policy."
  policy      = data.aws_iam_policy_document.sqs[0].json
}

resource "aws_iam_user_policy_attachment" "sqs" {
  count = var.queue_id != null ? 1 : 0

  user       = aws_iam_user.user.name
  policy_arn = aws_iam_policy.sqs[0].arn
}

resource "aws_iam_role_policy_attachment" "sqs" {
  count = var.queue_id != null ? 1 : 0

  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.sqs[0].arn
}
