output "s3_notification_details" {
  description = "The details of the S3 notification."

  value = {
    queue_arn = one(aws_sqs_queue.queue[*].arn)
    user_arn  = aws_iam_user.user.arn
    role_arn  = aws_iam_role.role.arn
  }
}
