resource "aws_s3_bucket_notification" "bucket_notification" {
  count = var.queue_id != null ? 1 : 0

  bucket = var.bucket_name

  queue {
    id            = var.queue_id
    queue_arn     = aws_sqs_queue.queue[0].arn
    events        = ["s3:ObjectCreated:*"]
    filter_prefix = var.queue_filter_prefix
    filter_suffix = var.queue_filter_suffix
  }

  depends_on = [
    aws_sqs_queue_policy.queue[0]
  ]
}
