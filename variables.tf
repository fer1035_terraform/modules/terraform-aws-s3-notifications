variable "bucket_name" {
  description = "The name of the S3 bucket."
  type        = string
}

variable "queue_id" {
  description = "A unique ID to identify the SQS queue. Leaving this as \"null\" will disable the queue."
  type        = string
  default     = null
}

variable "queue_filter_prefix" {
  description = "A prefix to filter the events that should trigger the SQS queue."
  type        = string
  default     = null
}

variable "queue_filter_suffix" {
  description = "A suffix to filter the events that should trigger the SQS queue."
  type        = string
  default     = null
}

variable "user_path" {
  description = "The path for the IAM user."
  type        = string
  default     = "/"
}

variable "assume_role_principals" {
  description = "The ARNs and / or IDs of the principals that can assume the role."
  default     = []
}

variable "external_id" {
  description = "The external ID to use in the role's trust relationship."
  type        = string
  default     = null
}
